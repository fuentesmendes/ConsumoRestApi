﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using System.Net.Http;
using System.IO;

namespace CONSUMO
{

    class Program
    {
        static void Main(string[] args)
        {
            string urlSMS = "http://saemcolombia.com.co:8009/send?";
            string usuario = "dmendez";
            string pass = "123456";
            string NroCelular = "573112782195";
            string Mensaje = "Te estoy observando... Sé por donde vas y a donde vas...";
            string desde = "seamco";
            string ruta = urlSMS;
            string param = "username=" + usuario + "&password=" + pass + "&to=" + NroCelular + "&content=" + Mensaje + "&from=" + desde;
            string respuesta;
            try
            {
                HttpWebRequest request = WebRequest.Create(ruta) as HttpWebRequest;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";

                // Metodo modificado
                string postData = param;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentLength = byteArray.Length;
                using (Stream dataStream = request.GetRequestStream())
                {
                    using (StreamWriter stmw = new StreamWriter(dataStream))
                    {
                        stmw.Write(postData);
                    }
                }

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    respuesta = reader.ReadToEnd();
                }

                Console.Write("Mensaje enviado con exito...");
                Console.Read();

            }
            catch (Exception ex)
            {
                Console.Write("El error es : " + ex);
                Console.Read();
            }


        }
    }
}
